# Lab 5: Introduction to _CSS_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## LAB 5 Objectives

What should you learn from codes & documentation in this sub-directory?

- Understand how to write CSS
- Have knowledge about static files in Django
- Understand the use of selector on CSS

## End Result
- Create a TODO List with a corresponding CSS

## Checklist

1.  Create a CSS for TODO List
    1. [ ] I've added a new bootstrap class on Navbar, that makes the Navbar static and located on top of the page.
    2. [ ] I've loaded the object `Todo` on `views.py` and have inputted it into response.
    3. [ ] I've made a section tag with `id=‘my-list’` on `lab_5.html`.
    4. [ ] I've made a div tag with `class=‘flex’` inside my-list section.
    5. [ ] I've made a child div with `class=‘flex-item’` from flex.
    6. [ ] I've done looping on `todo` data from `views.py` into flex-item. The `TODO List` which is rendered on flex-item has title, created_date and description.
    7. [ ] I've added my own implementation of flex display into the div CSS as a new rule, which i get help from https://css-tricks.com/snippets/css/a-guide-to-flexbox/. **Caution, you are prohibited from using css framework for flex.**
    8. [ ] I've set so that for desktop >= 768 px have 4 flex-item in one row. Surplus data will be appended on a new row.
    [ScreenShot 1](https://drive.google.com/file/d/0BzEo5TOpZj0VSXIyVTN1NkZHTXc/view?usp=sharing)
    9. [ ] I've set so that for mobile < 768 px only have 1 flex-item in each row.
    [ScreenShot 2](https://drive.google.com/file/d/0BzEo5TOpZj0VcDNTTXFkb0hwblU/view?usp=sharing)
    10. [ ] I've made sure that the css rule that i made is responsive for mobile. Hint: Use the Toggle Device Mode feature on browser.
    
2.  Ensure that you have good _Code Coverage_.
    1. [ ] If you haven't configure Gitlab to show your _Code Coverage_, please read WIKI Page [Show Code Coverage in Gitlab](https://gitlab.com/PPW-2017/ppw-lab-ki/wikis/show-code-coverage).
    2. [ ] I've reached 100% _Code Coverage_.

###  Challenge Checklist
Choose one:
1. [ ] I've added the `box-shadow` effect and moving up animation for `5px` on every flex-item when `hovered`.
2. [ ] I've implemented the delete `todo` function to delete the already created `todo` with only a push of a button for every flex-item.
3. [ ] I've added the `hover` effect for _button delete_. So, _button delete_ will only appear when one `to do` is `hovered`.
4. [ ] I've made a _Functional Test_ and _Unit Test_ to do _Testing_ on `delete` button.
5. [ ] I've inserted this code into `lab_5/tests.py` class `Lab5FunctionalTest` method `test_input_todo`, and _solve_ Test `test_input_todo` which has been appended with this code :
    
    ```python
        # check the returned result
            assert 'Berhasil menambahkan Todo' in selenium.page_source
<<<<<<< HEAD
    ```

## Creating a TODO List Page

1. Run your Virtual Environment
2. Install the new tools from `requirement.txt`
   Please modify your requirement.txt, so you have:
    ```txt
astroid==1.5.3
colorama==0.3.9
coverage==4.4.1
dj-database-url==0.4.2
Django==1.11.4
gunicorn==19.7.1
isort==4.2.15
lazy-object-proxy==1.3.1
mccabe==0.6.1
psycopg2==2.7.3.1
pylint==1.7.2
pytz==2017.2
selenium==3.5.0
six==1.10.0
whitenoise==3.3.0
wrapt==1.10.11

    ```
    In the new requirement.txt, you will have a selenium library installed.

    >pip install -r requirement.txt

    >List of new tools:
    >1. dj-database-url==0.4.2 
    >2. psycopg2==2.7.3.1
    >3. selenium==3.5.0
    >4. whitenoise==3.3.0
    
3. Make new _apps_ named `lab_5`
4. Insert `lab_5` into `INSTALLED_APPS`
5. Make new _Test_ inside `lab_5/tests.py` : 
    ```python
        from django.test import TestCase
        from django.test import Client
        from django.urls import resolve
        from .views import index, add_todo
        from .models import Todo
        from .forms import Todo_Form
        
        # Create your tests here.
        class Lab5UnitTest(TestCase):
        
            def test_lab_5_url_is_exist(self):
                response = Client().get('/lab-5/')
                self.assertEqual(response.status_code, 200)
        
            def test_lab5_using_index_func(self):
                found = resolve('/lab-5/')
                self.assertEqual(found.func, index)
        
            def test_model_can_create_new_todo(self):
                # Creating a new activity
                new_activity = Todo.objects.create(title='Doing Lab PPW', description='Doing lab_5 PPW')
        
                # Retrieving all available activity
                counting_all_available_todo = Todo.objects.all().count()
                self.assertEqual(counting_all_available_todo, 1)
        
            def test_form_todo_input_has_placeholder_and_css_classes(self):
                form = Todo_Form()
                self.assertIn('class="todo-form-input', form.as_p())
                self.assertIn('id="id_title"', form.as_p())
                self.assertIn('class="todo-form-textarea', form.as_p())
                self.assertIn('id="id_description', form.as_p())
        
            def test_form_validation_for_blank_items(self):
                form = Todo_Form(data={'title': '', 'description': ''})
                self.assertFalse(form.is_valid())
                self.assertEqual(
                    form.errors['description'],
                    ["This field is required."]
                )
            def test_lab5_post_success_and_render_the_result(self):
                test = 'Anonymous'
                response_post = Client().post('/lab-5/add_todo', {'title': test, 'description': test})
                self.assertEqual(response_post.status_code, 302)
        
                response= Client().get('/lab-5/')
                html_response = response.content.decode('utf8')
                self.assertIn(test, html_response)
        
            def test_lab5_post_error_and_render_the_result(self):
                test = 'Anonymous'
                response_post = Client().post('/lab-5/add_todo', {'title': '', 'description': ''})
                self.assertEqual(response_post.status_code, 302)
        
                response= Client().get('/lab-5/')
                html_response = response.content.decode('utf8')
                self.assertNotIn(test, html_response)
    ```
    
6. _Commit_ then _Push_ your work, then you can see your _UnitTest_ and you will see an _error_
7. Create a new URL Configuration inside `praktikum/urls.py` for `lab_5`
    ```python
        ........
        import lab_5.urls as lab_5
        
        urlpatterns = [
            .....
            url(r'^lab-5/', include(lab_5, namespace='lab-5')), 
        ]
    ```
8. Create a new URL Configuration inside `lab_5/urls.py`
    ```python
        from django.conf.urls import url
        from .views import index, add_todo
        
        urlpatterns = [
            url(r'^$', index, name='index'),
            url(r'^add_todo', add_todo, name='add_todo'),
        ]
    ```
9. Create a new _Models_ for `Todo` inside `lab_5/models.py`
    ```python
        from django.db import models

        class Todo(models.Model):
            title = models.CharField(max_length=27)
            description = models.TextField()
            created_date = models.DateTimeField(auto_now_add=True)
    ```
10. Run the command `makemigrations` and `migrate`
11. Create a new file `forms.py` and insert the code:
    ```python
    from django import forms

    class Todo_Form(forms.Form):
        error_messages = {
            'required': 'Please fill this input',
        }
        title_attrs = {
            'type': 'text',
            'class': 'todo-form-input',
            'placeholder':'Title'
        }
        description_attrs = {
            'type': 'text',
            'cols': 50,
            'rows': 4,
            'class': 'todo-form-textarea',
            'placeholder':'Description'
        }

        title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
        description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
    ```
12. Create a new `index` and `add_todo` function which will _handle_ URL from `lab_5`
    ```python
    from django.shortcuts import render
    from django.http import HttpResponseRedirect
    from .forms import Todo_Form
    from .models import Todo

    # Create your views here.
    response = {}
    def index(request):    
        response['author'] = "" #TODO Implement yourname
        todo = Todo.objects.all()
        response['todo'] = todo
        html = 'lab_5/lab_5.html'
        response['todo_form'] = Todo_Form
        return render(request, html, response)

    def add_todo(request):
        form = Todo_Form(request.POST or None)
        if(request.method == 'POST' and form.is_valid()):
            response['title'] = request.POST['title']
            response['description'] = request.POST['description']
            todo = Todo(title=response['title'],description=response['description'])
            todo.save()
            return HttpResponseRedirect('/lab-5/')
        else:
            return HttpResponseRedirect('/lab-5/')
    ```
13. Make `lab_5.css` inside `lab_5/static/css` (You can create the folder if it is not available)
    ```css
    body{
      margin-top: 70px;
    }
    
    /* Custom navbar style */
    .navbar-static-top {
      margin-bottom: 19px;
    }
    .navbar-default .navbar-nav>li>a {
        cursor: pointer;
    }
    
    /* Textarea not resizeable */
    textarea {
        resize:none
    }
    section{
      min-height: 600px;
    }
    /* animation for title */
    .main-title{
      -webkit-animation: colorchange 1s infinite;
      -webkit-animation-direction: alternate;
      text-align: center;
    }
    
    /* colorchange animation */
    @-webkit-keyframes colorchange {
        0% {
            -webkit-text-stroke: 5px #0fb8ad;
        letter-spacing: 0;
        }
      50% {
        -webkit-text-stroke: 7.5px  #1fc8db;
      }
        100% {
            -webkit-text-stroke: 10px  #2cb5e8;
        letter-spacing: 18px;
        }
    }
    /* styling wrapper form */
    #input-list{
      background: linear-gradient(to bottom right, #606062, #393939);
    }
    /* styling form */
    #input-list form{
      width: 400px;
      margin: 50px auto;
      text-align: center;
      position: relative;
      z-index: 1;
      background: white;
      border: 0 none;
      border-radius: 3px;
      box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
      padding: 20px 30px;
      box-sizing: border-box;
      position: relative;
    }
    /* title form styling, what does '>' means ? */
    #input-list form > h2{
      font-size: 1.3em;
      text-transform: uppercase;
      color: #2C3E50;
      margin-bottom: 10px;
    }
    /* input and textarea form styling, what does ',' means ? */
    #input-list form > .todo-form-input, #input-list form > .todo-form-textarea{
      padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 13px;
    }
    /* styling footer */
    footer p{
      text-align: center;
      padding-top: 10px;
    }

    /* responsive styling, max-width indicates that this css rule will only works with a maximal of 768px screen. If it's more than 768px, then the rule will be ignored. */
    @media only screen and (max-width: 768px) {
      #input-list form {
          width: 290px;
      }
    }
    
    #input-list{
      background: linear-gradient(to bottom right, #606062, #393939);
    }
    #input-list form{
      width: 400px;
      margin: 50px auto;
      text-align: center;
      position: relative;
      z-index: 1;
      background: white;
      border: 0 none;
      border-radius: 3px;
      box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
      padding: 20px 30px;
      box-sizing: border-box;
      position: relative;
    }
    #input-list form > h2{
      font-size: 1.3em;
      text-transform: uppercase;
      color: #2C3E50;
      margin-bottom: 10px;
    }
    #input-list form > .todo-form-input, #input-list form > .todo-form-textarea{
      padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 13px;
    }
    
    #my-list{
      background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
    }
    #my-list .my-list-title{
      font-size: 40px;
      margin-bottom: 1em;
      color: white;
      text-shadow: 3px 3px 0 #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
      text-align: center;
    }
    
    .to-do-list{
      text-align: center;
      background: #fff;
      position: relative;
      z-index: 15;
      margin-bottom: 60px;
      -webkit-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      -moz-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      -o-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      transition: 0.5s all;
      -webkit-transition: 0.5s all;
      -moz-transition: 0.5s all;
      -o-transition: 0.5s all;
    }
    .to-do-list:after{
        content:"";
        display:block;
        height:25px; /* Which is the padding of div.container */
        background: #fff;
    }
    .flex{
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: flex-start;
      align-items: flex-start;
      align-content: flex-start;
      margin-right: -15px;
      margin-left: -15px;
    }
    .flex-item{
      padding: 0 15px;
      flex-grow: 0;
      flex-shrink: 0;
      flex-basis: 25%;
    }
    
    .to-do-list .to-do-list-title {
      font-size: 20px;
      font-weight: 700;
      padding: 10px 10px 0;
    }
    .to-do-list .to-do-list-date-added {
      font-size: 12px;
      padding: 0 10px;
    }
    .to-do-list .to-do-list-description {
      padding: 10px 25px;
      z-index: 15;
      background: #fff;
      height: 125px;
      text-align: justify;
      overflow: auto;
    }
    .to-do-list .to-do-list-delete {
      background: #e45;
      color: #fff;
      padding: 10px;
      font-size: 16px;
      width: 100%;
      height: 50px;
      bottom: 1px;
      position: absolute;
      cursor: pointer;
      z-index: -1;
      -webkit-border-radius: 0 0px 2px 2px;
      -moz-border-radius: 0 0px 2px 2px;
      -o-border-radius: 0 0px 2px 2px;
      border-radius: 0 0px 2px 2px;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
    }
    .to-do-list .to-do-list-delete {
      bottom: -50px;
    }

    ```
14. Create `base.html` inside `lab_5/templates/lab_5/layout` (You can create the folder if it is not available) 
    ```html
    {% load staticfiles %}
    {% load static %}
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="LAB 5">
        <meta name="author" content="{{author}}">
        <!-- bootstrap csss -->
        

        <title>
            {% block title %} Lab 5 By {{author}} {% endblock %}
        </title>
    </head>

    <body>
        <header>
            {% include "lab_5/partials/header.html" %}
        </header>
        <content>
                {% block content %}
                    <!-- content goes here -->
                {% endblock %}
        </content>
        <footer>
            <!-- TODO Block Footer dan include footer.html -->
            {% block footer %}
            {% include "lab_5/partials/footer.html" %}
            {% endblock %}
        </footer>

        <!-- Jquery n Bootstrap Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="application/javascript">
          $(function() {
              $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html, body').animate({
                      scrollTop: target.offset().top - 55
                    }, 1000);
                    return false;
                  }
                }
              });
            });
        </script>
    </body>
    </html>
    ```
15. To access _bootstrap_ and _css_ that you have made, add a link to `lab_5.css` on `base.html`
    ```html
    <head>
    ...
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{% static 'css/lab_5.css' %}" />
    ...
    </head>
    ```
16. Make `header.html` and `footer.html` on folder `templates/lab_5/partials`

    ```html
    <!-- templates/lab_5/partials/header.html -->
    <nav>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Home</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- LINK INTO table, and link into home#form -->
            <li><a href="{% url 'lab-5:index' %}#input-list">Input Todo</a></li>
            <li><a href="{% url 'lab-5:index' %}#my-list">My List</a> </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    ...
    ```
    ```html
    <!-- templates/lab_5/partials/footer.html -->
    <!-- TODO, CREATE THIS FILE AND ADD copyright symbol -->
    <p>Made with Love by {{author}}</p>

    ```
17. Make `lab_5.html` inside `lab_5/templates/lab_5/`
    ```html
    {% extends "lab_5/layout/base.html" %}

    {% block content %}
    <h1 class="main-title">TODOLIST</h1>
    <hr/>
    <section name="input-list" id="input-list">
        <div class="container">
            <form id="form" method="POST" action="{% url 'lab-5:add_todo' %}">
                <h2>Input Todo</h2>
                {% csrf_token %}
                {{ todo_form }}
                <input id="submit" type="submit" class="btn btn-lg btn-block btn-info" value="Submit">
                <br>
            </form>
        </div>
    </section>
    <section name="my-list" id="my-list">
        <div class="container">
            <h2 class="my-list-title">My List</h2>
            <div class="flex">
                {% if todo %}
                    {% for data in todo %}
                        <div class="flex-item">
                            <div class="to-do-list">
                                <div class="to-do-list-title">
                                    {{data.title}}
                                </div>
                                <div class="to-do-list-date-added">
                                    {{data.created_date}}
                                </div>
                                <div class="to-do-list-description">
                                    {{data.description}}
                                </div>
                                <div class="to-do-list-delete">
                                    <div class="to-do-list-delete-button" data-id="{{data.id}}">Delete</div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                {% else %}
                <div class="alert alert-danger text-center">
                    <strong>Oops!</strong> There are no TODO data.
                </div>
                {% endif %}
            </div>
        </div>
    </section>


    {% endblock %}        
    ```
18. Modify so the `static file` are able to be deployed to heroku.
    1. Install whitenoise with this command:
        > pip install whitenoise
    2. Pada `settings.py` make sure that the middleware and storage settings looks like this:
        ```python
        MIDDLEWARE_CLASSES = (
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/
            'whitenoise.middleware.WhiteNoiseMiddleware',
            ...
        ```
        ```python
            ...
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/

            STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
        ```
    3. On build process, heroku will automaticly run `python manage.py collecstatic --noinput`. If there's a fail on build process, then run `heroku config:set DEBUG_COLLECTSTATIC=1` to know which error happens.
    4. If you are having trouble on deploy process. You can visit this link. [Help Deploy Heroku](https://devcenter.heroku.com/articles/django-assets)

19. _Commit_ and _push_. Then, continue to testing part. 

## Testing using Selenium

#### Testing using selenium on local environment

1.  Create a new `class` on `tests.py` as followed.

    ```python
    .............
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.chrome.options import Options
    
    
    
    # Create your tests here.
    class Lab5FunctionalTest(TestCase):
    
        def setUp(self):
            chrome_options = Options()
            self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
            super(Lab5FunctionalTest, self).setUp()
    
        def tearDown(self):
            self.selenium.quit()
            super(Lab5FunctionalTest, self).tearDown()
    
        def test_input_todo(self):
            selenium = self.selenium
            # Opening the link we want to test
            selenium.get('http://127.0.0.1:8000/lab-5/')
            # find the form element
            title = selenium.find_element_by_id('id_title')
            description = selenium.find_element_by_id('id_description')
    
            submit = selenium.find_element_by_id('submit')
    
            # Fill the form with data
            title.send_keys('Mengerjakan Lab PPW')
            description.send_keys('Lab kali ini membahas tentang CSS dengan penggunaan Selenium untuk Test nya')
    
            # submitting the form
            submit.send_keys(Keys.RETURN)
    ```
2.  You can see in `tests.py`, there is import library `selenium`
    ```python
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.chrome.options import Options
    ...
    ```
3. Install `selenium` with command:
    > pip install selenium

4. Download `chromedriver` from this url. Make sure that you download the correct OS version.
[Download chromedriver](https://chromedriver.storage.googleapis.com/index.html?path=2.32/)

5. `Unzip` chromedriver into your project folder. (`chromedriver.exe` on Windows and `chromedriver` on Linux and Mac). This is the folder structure (example in windows)

            lab-ppw
    		├──lab_1
    		├──lab_2
    		├──...
            ├──lab_5
            ├── manage.py
            ├──...
            ├── README.md
            ├── chromedriver.exe
            ├──...

6. Modify your `tests.py` as followed. (example in windows)
    ```python
    ...
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        super(Lab5UnitTest, self).setUp()
    ...
    ```
7. Make sure you have chrome browser.
8. Run `./manage.py collectstatic`
9. Run `./manage.py runserver`
10. Open new terminal, and run test with `./manage.py test`
11. Chrome browser will open and do test automatically.

#### Testing using Selenium on Gitlab-CI

1. Make sure that you have done step 1 - 4 of **Testing using selenium on local environment** 

2. Modify `tests.py` as follow:
    ```python
    ...
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab5UnitTest, self).setUp()
    ...
    ```

3. Ensure that the file `.gitignore` have this sentence on the last line.

    ```bash
    ...
    #ignore chromedriver
    chromedriver
    chromedriver.exe
    /pratikum/static/*
    ```
4. _Commit_ and _push_.

Explanation about Selenium :
[Click me](http://www.seleniumhq.org/)


## Introduction about CSS

#### What is CSS

Cascading Style Sheets (CSS) is a language which is used to describe the interface and format of a website that is written on a markup language (ex: HTML). It is used to make website interfaces more attractive

#### CSS writing format

Commonly, CSS can be written as followed:
```css
    selector {
        properties:value;
    }
```

There are 3 style of CSS writing format

**1. Inline Styles**

Inline styles is a style that is written inside an element and only works for that element. It is commonly used to configure a unique look on a single element. The definition are written by adding attribute style that has CSS property and desired values.

```html
    ...
    <div id=”main”
        style=”background­-color: #f0f0f0;
        color: red;
        font­-family: georgia;
        font-­size: 0.8em;”>
        ...
    </div>
    ...
```

Attribute style pada `div` di atas merupakan contoh penulisan inline style. Hal ini akan menyebabkan `div` tersebut akan memiliki properties seperti value yang diberikan.
The above `div` attribute style is an example of inline style writing format. The attribute style will change the properties of the `div` with the input thats given inside it.

**2. Internal Style Sheet**

Internal style sheet are defined inside the element `style` on `head` element. It is commonly used to make a unique page that has different style. You are going to try to make the same interface like the inline style example before by using Internal style sheet. Delete the attribute style that you have made before, then make a `style` element on `head` element as follows:

```html
    ...
    <head>
        ...
        <style>
            #main {
                background­-color: #f0f0f0;
                color: red;
                font­-family: georgia;
                font­-size: 0.8em;
            }
        </style>
        ...
    </head>
    <body>
        <div id="main">
            ...
        </div>
    </body>
    ...
```

**3. External Style Sheet**

External style sheet are defined outside the HTML document. External style sheet allows to change the interface of more than one page by giving a reference to a CSS file. To learn more about External Style Sheet, you can follow the tutorial below.

First, you're going to make `tutorial.html` which will point to `tutorial.css` with the folder structure like this.

        lab_5
        ├──migrations
        ├──templates
            ├──lab_5
                ├── tutorial.html
        ├──static
            ├── css
            │   ├── tutorial.css


Then, insert the following code into `tutorial.css`.

```css
    #main {
        background­-color:#f0f0f0;
        color:red;
        font­-family:georgia;
        font­-size:0.8em;
    }
```

Lastly, you can make your HTML template point to the CSS file (`tutorial.css`)

```html
{% load staticfiles %}
<html>
    <head>
        <title>Tutorial CSS Yay</title>
        <link rel="stylesheet" href="{% static 'css/tutorial.css' %}">
    </head>
    <body>
        <div>
            <h1>Tutorial CSS Yay</h1>
        </div>
        <div id="main">
            <div>
                <p>published: 31 September 2017</p>
                <h1><a href="">My CSS Tutorial</a></h1>
                <p>This is an easy tutorial!</p>
             </div>
        </div>
    </body>
</html>
```

This can happen because CSS is a `static files` in Django. You will learn more about `static files` in the next section

>If there are more than one style that is defined for an element, then the style that will be applied is the one that has higher priority. Below is the list of style's priority, number 1 has the highest priority.
>1. Inline style
>2. External and Internal style sheets
>3. Browser default

#### Static files in Django

In Django framework, there are files that are called as static files. `static files` is a collection of files that supports HTML on a website. The example of `static files` is CSS, Javascript and Image. Configuration of `static files` are located inside `settings.py`

```python
    ...

    # Static files (CSS, JavaScript, Images)
    # httpsdocs.djangoproject.comen1.9howtostatic-files
    STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
    STATIC_URL = 'static'

    ...
```
On `settings.py` theres `STATIC_ROOT` which define _absolute path_ to the `static files` directory when it runs the command `collectstatic` on project and `STATIC_URL`, which is a URL that can be accessed by public to obtain the `static files`, are available.

`collectstatic` command is a command that is used to gather `static files` from all `app` to make it easier to access all `app`

You can learn more about static files here. 
[static files](httpsdocs.djangoproject.comen1.11howtostatic-files)

## Selector in CSS

For the tutorial, you could re-use `tutorial.html` and `tutorial.css`. You will learn 3 type of selector on CSS, which is element selector, class selector and id selector.

**1. Element Selector**

Element selector uses html tag as a selector to change properties that is located inside the tag. In this part of tutorial, you are going to use element selector as follows in `tutorial.css`.

```css
    h1 {
        color:#FCA205;
        font-family:'Monospace';
        font-style:italic;
    }
```

When you refresh your website, you can see that `h1` element has changed.

**2. Id Selector**
 
Id selector uses id at a tag as the selector

You can add an id inside HTML template as follows (id must be unique).
```html
    ...
    <body>
        <div id="header">
            <h1>Tutorial CSS Yay</h1>
        </div>
    ...
    </body>
```
Then, use that id as a selector at `tutorial.css`

```css
    #header {
        background-color:#F0F0F0;
        margin-top:0;
        padding:20px 20px 20px 40px;
    }

```

You can see that the interface has changed. You can add other id selector to change properties.

**3. Class Selector**

Next, you can use class selector to change your HTML template interface `tutorial.html`
Add a few class on HTML tag

```HTML
    ...
        <div id="main">
            <div class="content_section">
                <p class="date">published: 31 September 2017</p>
                <h2><a href="">My CSS tutorial</a></h2>
                <p id="content_1">Yay, this is an easy tutorial!</p>
            </div>
            <div class="content_section">
                <p class="date ">published: 32 September 2017</p>
                <h2><a href="">Your CSS tutorial</a></h2>
                <p id="content_2">Yay this is an easy tutorial!</p>
            </div>
            <div class="content_section">
                <p>published: 33 September 2017</p>
                <h2><a href="">CSS Tutorial for all</a></h2>
                <p id="content_3">Yay this tutorial is not hard!</p>
            </div>
        </div>
    ...
```
Then, add class selector at css template `tutorial.css`

```css
    .content_section {
        background-color:#3696E1;
        margin-bottom: 30px;
        color: #000000;
        font-family: cursive;
        padding: 20px 20px 20px 40px;
    }
```

You can see that the interface has changed. You can add other class selector to change properties.
 
 > Difference between id selector and class selector
 > Id Selector use #[id_name] format and always starts with #
 > Class selector uses .[class_name] format and always start with .
 
 To deepen your knowledge about CSS Selector Reference, you can read more information below:
 https://www.w3schools.com/cssref/css_selectors.asp

## Tips & Tricks CSS

#### Introductions to Combinators

After knowing selector in CSS, you can learn about `Combinators` in CSS. `Combinators` is something that explains a relationship between element, class or id inside CSS. There are 4 `Combinators` in CSS :

**1. Descendant selector (space)**

With this `Combinators`, you can choose all its _descendant_ element that follows the pattern. Examples:
    
```html
...
    <div>
        <p>Something here</p>
        <span><p>something special</p></span>
    </div>
    <p> Another thing </p>
    <p> Another thing, again </p>
...
```
    
```css
    div p {
        background-color: red;
    }
```

From the code above, then all `p` that is the descendant of `div` tag will be chosen.

**2. Child selector (>)**

Just like _descendant selector_, _child selector_ only choose the first descendant _(child)_ from the element that follows the pattern.

```css
    div > p {
        background-color: red;
    }
```
You can try the code above and see the difference

**3. Adjacent sibling selector (+)**

_Adjacent sibling selector_ only choose one element with the same level _(sibling)_ that follows the pattern.

```css
    div + p {
        background-color: red;
    }
```

**4. General sibling selector (~)**

While _general sibling selector_ will chose all _sibling_ that follows the pattern.

```css
    div ~ p {
        background-color: red;
    }
```

#### Introduction to CSS Pseudo-class

Pseudo-class is used to define specific state from an element. Here is an example of some pseudo-class

- :active  chooses an active element
- :cheked  chooses an element that is checked
- :disabled  chooses an element that is disabled
- :enabled  chooses an enabled element
- :link   chooses a link that has never been visited
- :hover  chooses the element when the mouse is hovering on it
- :visited  chooses a link that has been visited before

commonly, pseudo-class are written like this.
```css
selector:pseudo-class {
    properties:value;
}
```

#### Perbedaan Margin, Border dan Padding 
#### Differences between Margin, Border and Padding

<img src="http://1.bp.blogspot.com/-RUtAOT_XsS4/UywVWVG8ohI/AAAAAAAAAL4/HDf0cL0GbGs/s1600/css+margin+padding.gif" alt="margin_padding" width="500" height="300"/>

Here you can see the differences between margin, border and padding for an element's properties

#### Introduction to Bootstrap

There are tons of CSS framework that is used nowadays, one of them is Bootstrap CSS. Bootstrap CSS provides classes that is often used in developing a websites. Bootstrap provides classes such as navbar, card, footer, carousel and others. Futhermore, Bootstrap also provide lots of useful feature, one of them is grid system to split the website page to make developing easier and resulting in interesting pages.

You can learn more about Bootstrap CSS here:
[Bootstrap](httpsgetbootstrap.comdocs3.3csshttpsgetbootstrap.comdocs3.3css)

#### Responsive Web Design

Responsive web design is an approach for website interface so that it will look good on all devices (_desktop_,_tablets_, or _phones_). Responsive Web Design doesn't change the content, it only change the interface so that every device will look the same or looks good. Responsive design uses CSS to _resize_ the element.

 >Content is like water
 >
 >-- Responsive Web Design
 
 To open the Toggle Device Mode feature at chrome browser:
 
 Windows/Linux   : `CTRL + SHIFT + M`
 Mac             : `Command + Shift + M`

More info about Responsive Web Design:
[Responsive Web Design](httpsdevelopers.google.comwebfundamentalsdesign-and-uiresponsive)

### Additional Info

For full implementation of all _Checklist_ can be seen in [here](https://igun-lab.herokuapp.com/lab-5/)

Credit by [@EdisonTantra](https://gitlab.com/EdisonTantra), [@falsewaly](https://gitlab.com/falsewaly), 
[@fajrinajiseno](https://gitlab.com/fajrinajiseno), and [@glory.finesse](https://gitlab.com/glory.finesse).

Verified by [@hafiyyan94](https://gitlab.com/hafiyyan94) and [@KennyDharmawan](https://gitlab.com/KennyDharmawan)

Translated by [@kakioshe](https://gitlab.com/users/kakioshe)


=======
    ```
>>>>>>> f1756ed5c1035108ef18d7d4cf4783ebd1b5757a
